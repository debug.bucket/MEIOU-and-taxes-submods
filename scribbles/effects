#############################
# List of EU IV Effects
#############################
clr_global_flag =
set_global_flag =

tooltip = { } #If you put tooltip = {} around an effect it only shows the tooltip and doesn't actually carry out the effect when the option is selected
custom_tooltip = # Creates a custom tooltip (as in words that the player will see when they look at the options effects). Found in elections, natives, and elsewhere
hidden_effect = {
 
}
highlight = yes #Used in an event option, this highlights the option. Vanilla uses it for options unlocked by ruler personalities.
required_personality = #Place this in an event option and it will add a tooltip that states the option is available due to having that personality

#Scripted Effects (defined in common\scripted_effects)
scripted_effects_name = yes

hide_ambient_object =
show_ambient_object =

#############################
# Other Country Effects
#############################
add_accepted_culture =
add_idea =
add_idea_group =
change_graphical_culture =
change_government =
change_primary_culture =
change_tag =
change_technology_group =
clear_global_event_target =
clear_global_event_targets =
clr_country_flag =
collapse_nation = yes
disband_rebels = #rebel type here, this should disband all rebels of this type in your lands
loan_size = 24  #Increases the amount of money loaned, loan size = 1 means each loan will be 1 months income, so loan size = 12 would be 12 months of income per loan.
remove_accepted_culture =
remove_country_modifier =
remove_idea =
remove_idea_group =
save_event_target_as =
save_global_event_target_as =
set_country_flag = #Can append a scope or event target to the end of a script flag, such as flag_name_@ROOT or flag_name_@event_target:targe_name
set_government_rank = 2 #1-3
set_primitive = no
set_revolution_target = #use ROOT, tag, FROM, etc.  Can also use xxx to make it so that no country is the revolution target (i.e. set_revolution_target = xxx )
swap_free_idea_group = yes
switch_tag =

add_country_modifier = {
    name = _
    duration = 365
}

#hidden country modifier, usually name of modifier is something timer, i.e. roman_fever_timer
hidden_effect = {
    add_country_modifier = {
        name = _
        duration = 3650
        hidden = yes
    }
}

#Can add a description of the duration, such as "the 18th century"
#Then (with a modifier titled "Prestige Loss") the tooltip will read "England will have Prestige Loss until the 18th century"
add_country_modifier = {
    name =
    duration = -1
    desc = "_"
}

country_event = { id = _ }
country_event = { id = _ tooltip = _ }
country_event = { id = _ days = 3 }
#Example event using all 3
country_event = { id = flavor_fra.3004 days = 10 tooltip = "flavor_fra.EVTTOOLTIP3002" }
#Not sure what random does here, I believe it randomizes the amount of days that it will take for the event to fire, but I haven't confirmed this yet
country_event = {
    id = _
    days = 1
    random = 5
}
#Example of all being used from flavorBOH events
country_event = {
    id = flavor_boh.3
    days = 2500
    random = 100
    tooltip = flavor_boh.2.3
}

change_price = {
    trade_goods =
    key =
    value =
    duration = -1
}
#Value is a modifier, so a value of 0.2 would increase the price of the good by 20%
#Can also have duration be the number of days it lasts until expiring

#The on_trigger is the trigger/left side argument, and each set of brackets below represent a left side argument for that trigger.
#Allows you to cut down on your IF brackets by applying a specific effect to each left side argument
#Kind of hard to explain, see the example below
trigger_switch = {
    on_trigger =
 
}
#Example
trigger_switch = {
    on_trigger = technology_group
    western = { set_variable = { which = test value = 1 } }
    eastern = { set_variable = { which = test value = 2 } }
    scandinavian_tech = { set_variable = { which = test value = 3 } }
    muslim = { set_variable = { which = test value = 4 } }
    indian = { set_variable = { which = test value = 5 } }
}

while = {
    limit = {
        check_variable = { which = variable_name value = 1 }
    }
    subtract_variable = { which = variable_name value = 1 }
    add_treasury = 5
}
#The while effect will not produce any tooltip. Also make sure you don't create an endless loop ( although there is a fail-safe at 100K loops)

#############################
# Adjust Values
#############################
add_adm_power = -50
add_corruption = 1
add_dip_power = -50
add_faction_influence = { faction = _ influence = 5 }
add_horde_unity = 5
add_inflation = 5
add_imperial_influence = 10
add_legitimacy = 10
add_liberty_desire = 5
add_mercantilism = 5
add_militarised_society = 20
add_mil_power = -50
add_prestige = -5
add_republican_tradition = -10
add_scaled_imperial_influence = 10 #Not sure of the difference between this and the regular adding of imperial influence
add_scaled_republican_tradition = -0.10 #Scaled to the country's election cycle
add_stability = -1
add_tariff_value = 0.05
add_treasury = 100
add_war_exhaustion = -10
add_westernisation = 1
add_years_of_income = 0.25
change_statists_vs_orangists = 0.25

#############################
# Advisors
#############################
create_advisor = #advisor type
kill_advisor = #random, type of advisor, probably can also use specific advisor id, I'm guessing kill advisor completely removes from game
remove_advisor = #random, type of advisor, probably can also use specific advisor id, I'm guessing remove advisor just puts advisor back in the pool.

define_advisor = {
    type =
    name = "_"
    skill = 2
}

#As of 1.11 we can now make female advisers
female = yes

#Can make an adviser cheaper with discount = yes
define_advisor = {
    type = 
    skill = 2
    discount = yes
}

#Can also add location of advisor home province, location = is the desired province id
#Example
define_advisor = {
    type = spymaster
    name = "Francis Walsingham"
    location = 235
    skill = 3
}

#############################
# AI Effects
#############################
add_ai_strategy = #never found in code
fixed_ai_strategy = #never found in code
rebuild_ai_attitudes = #never found in code
rebuild_ai_priorities = #never found in code

set_ai_personality = {
    personality =
    locked = yes/no
}

set_ai_attitude = {
    attitude =
    who =
    locked = yes/no
}

#############################
# Diplomatic - most accept TAG, ROOT, FROM, PREV, etc
#############################
add_historical_friend =
add_historical_rival =
add_opinion = { who = _ modifier = _ } #Adds an opinion modifier of the who = to the scope country
add_spy_network_from = { who = _ value = 25 }
add_spy_network_in = { who = _ value = -25 }
add_truce_with =
break_marriage =
break_union =
create_alliance =
create_guarantee =
create_march =
create_marriage =
create_protectorate =
create_union =
declare_war =
free_vassal = #never found in code
form_coalition_against =
inherit =
join_league =
leave_league =
release =
release_all_subjects = yes
release_vassal =
remove_fow = 3 #Remove Fog of War, not sure what the number is, maybe number of months
remove_historical_friend =
remove_historical_rival =
reverse_add_opinion = { who = _ modifier = _ } #Adds an opinion modifier of the scope country to the who =
vassalize =
white_peace =

#Gives the scope country a CB against the target
add_casus_belli = {
    target =
    type =
    months = 1825
}

#Gives the target a CB against the scope country
reverse_add_casus_belli = {
    target =
    type =
    months = 12
}

remove_casus_belli = {
    type =
    target =
}
#can also use reverse
reverse_remove_casus_belli

#Can also add a time frame on the opinion modifier
add_opinion = {
    who =
    modifier =
    years = 50
}

remove_opinion = {
    who =
    modifier =
}

reverse_remove_opinion = {
    who =
    modifier =
}

#Use a CB to declare war, who = the target (use tag, ROOT, etc) and casus_belli = the CB used
declare_war_with_cb = {
    who =
    casus_belli =
}

#Reverse effects are good for random country diplomacy
#Example
random_country = {
    limit = { is_neighbor_of = ROOT }
    reverse_add_casus_belli = {
        target = ROOT
        type = cb_insult
        months = 12
    }
}

#############################
# Estates, Factions, Parliaments, and Disasters
#############################
add_disaster_modifier = { name = key disaster = disaster_key duration = x }
add_disaster_progress = { disaster = _ value = _ }
add_faction = #name of faction
create_independent_estate = #name of estate
create_independent_estate_from_religion = #name of estate
dissolve_parliament = yes #Removes the Parliament mechanic from the country
end_disaster = #disaster name
reinstate_parliament = yes #Re-adds the Parliament mechanic to a country that had it dissolved
remove_faction = #name of faction

add_estate_influence_modifier = {
    estate =
    desc =
    influence = -10
    duration = 3650
}
add_estate_loyalty = {
    estate =
    loyalty = -15
}
add_estate_loyalty_modifier = {
    estate =
    desc =
    loyalty = 10
    duration = 7300
}

#############################
# HRE
#############################
add_hre_emperor_modifier =
add_hre_member_modifier =
dismantle_hre = yes
elector = #Grants elector status
enable_hre_leagues = yes
hre_inheritable = yes
imperial_ban_allowed = yes
internal_hre_cb = no
remove_hre_emperor_modifier =
remove_hre_member_modifier =
revoke_reform = #never found in code
set_allow_female_emperor = yes
set_hre_heretic_religion =  #name of religion
set_hre_religion = #name of religion
set_hre_religion_locked = yes
set_hre_religion_treaty = yes
set_in_empire = no #No will remove all scope's provinces from the empire, yes will add all scope's provinces

#############################
# Military
#############################
add_army_tradition = 10
add_manpower = -8   #can also be a decimal
add_navy_tradition = 10
add_sailors = 1000
add_yearly_manpower = -0.25
add_yearly_sailors = 1.5
change_unit_type =
create_admiral = 10
create_conquistador = 5
create_explorer = 5
create_general = 10
kill_leader = { type =  } #general, admiral, conquistador, explorer, random, or specific name

#As of 1.12, leaders no longer have to have a name
define_admiral = {
    name = "_"
    shock = 4
    fire = 4
    manuever = 5
}

define_conquistador = {
    name = "_"
    fire = 3
    shock = 3
    manuever = 3
    siege = 1
}

define_explorer = {
    name = "_"
    fire = 2
    shock = 2
    manuever = 6
    siege = 0
}

define_general = {
    name = "_"
    fire = 3
    shock = 3
    manuever = 3
    siege = 1
}

#Can add this to make a leader female
female = yes

#Add Units- can use province id, i.e. cavalry = 236
#Although vanilla uses unit_type = capital (i.e. infantry = capital) this effect is bugged and doesn't work properly.
artillery =
cavalry =
galley =
heavy_ship =
infantry =
light_ship =
transport =

#Can also add specific units with specific_unit_name = province_id
british_redcoat = 236
twodecker = 236

#The following allows you to build mercenary units- can use province id
mercenary_cavalry = 236
mercenary_infantry =  236

#############################
# Province Effects (Country Scope)
#############################
#Can probably also use tag, ROOT, PREV, etc. with these, not tested for all of them though
add_claim = #province id
add_core = #province id
add_territorial_core = #province id
discover_province = #province id, Confirmed by AKronblad that PREV works here
remove_core = #province id
remove_territorial_core =  #province id
set_capital = #province id
undiscover_province = #province id

#############################
# Random
#############################
#one use for random effects is to put in the immediate = { } for randomness, and then have options play off it
random = {
    chance = 50
    #Effect Here
}

random_list = {
    25 = {

    }
    25 = {

    }
    25 = {

    }
    25 = {

    }
}

#############################
# Religion
#############################
add_authority = 5
add_church_aspect = #Aspect's name
add_church_power = 20
add_devotion = 10
add_doom = -5
add_fervor = 10
add_karma = 10
add_papal_influence = 1
add_patriarch_authority = 0.25  #this is a percentage
add_piety = 0.1
add_reform_desire = 0.01 #this is a percentage
change_personal_deity = #deity's name
change_religion =
enable_religion =  #religion name
excommunicate = #ROOT, TAG, FROM, PREV, etc, if target is already excommunicated then it will be ended
force_converted = yes
remove_church_aspect = #Aspect's name or random
remove_religious_reforms = 2
set_karma = 10
set_papacy_active = no

#############################
# Ruler and Heir
#############################
add_heir_claim = -20
add_heir_personality = #personality name
add_heir_support = 10
add_queen_personality = #personality name, does not have a tooltip
add_ruler_personality = #personality name
change_adm = 1
change_dip = 1
change_mil = 1
change_consort_regent_to_ruler = yes
change_heir_adm = 1
change_heir_dip = 1
change_heir_mil = 1
clear_scripted_personalities = yes #Removes all ruler personalities. Does not remove heir or consort personalities
clr_consort_flag =
clr_heir_flag =
clr_ruler_flag =
convert_female_ruler_to_general = yes
convert_female_heir_to_general = yes
convert_heir_to_general = yes
convert_ruler_to_general = yes
exile_heir_as = #unique text string, similar to flags.  Also like flags, we can use dynamic strings with @ROOT, TAG, FROM, PREV, etc.
exile_ruler_as = #unique text string, similar to flags.  Also like flags, we can use dynamic strings with @ROOT, TAG, FROM, PREV, etc.
kill_heir = yes
kill_ruler = yes
remove_consort = yes
remove_heir = yes
remove_heir_personality = #personality name
remove_queen_personality = #personality name, does not have a tooltip
remove_ruler_personality = #personality name
set_dynasty = #set dynasty without killing the ruler. Can use tags and a specific dynasty, probably ROOT, FROM, PREV, etc as well
set_consort_flag =
set_heir = #unique text string previously set with the exile_heir_as effect.
set_heir_flag =
set_ruler = #unique text string previously set with the exile_ruler_as effect. Setting a ruler will remove any existing heir
set_ruler_flag =

#Ruler modifiers last until the ruler's death.  Can use either of the two formats below, the -1 duration isn't needed.
add_ruler_modifier = { name = _ }
add_ruler_modifier = {
    name = "_"
    duration = -1
}

#Could also be a specific dynasty or FROM if fired from someone else's event, could also use THIS and PREV
#Can also just define one or two ruler stats, leaving rest up for chance
define_heir = {
    claim = 100
    dynasty = ROOT
    name = "Alexander"
    adm = 3
    dip = 3
    mil = 3
}
#Can use fixed = yes to make those numbers the heir's exact stats

#As of 1.4 can now set the age of heir #Tested and confirmed by AKronblad
define_heir = {
    claim = 100
    dynasty = ROOT
    name = "Louis"
    adm = 3
    dip = 3
    mil = 3
    age = 12
}

#Setting heir birth date
define_heir = {
    adm = 1
    dip = 2
    mil = 1
    birth_date = 1440.2.22
}

#Add this to ensure that the heir is male
define_heir = {
    male = yes
}

#Add this for a female heir
define_heir = {
    female = yes
}

#Can hide the stats of the new heir
define_heir = {
    hidden = yes
}

#These were added in 1.18
define_heir = {
    dynasty = original_dynasty
    max_random_adm = 5
    max_random_dip = 5
    max_random_mil = 5
}

#Adding this ensures that a consort isn't spawned with the heir (ex. the Child in the Reeds event)
define_heir = {
    no_consort_with_heir = yes
}

define_heir_to_general = {
    fire = 4
    shock = 4
    manuever = 3
    siege = 1
}

#Random but numbers must be at least this high
define_ruler = {
    adm = 1
    dip = 1
    mil = 1
}

#fixed = yes means that these are the exact numbers of the ruler
define_ruler = {
    mil = 1
    adm = 4
    dip = 1
    fixed = yes
}

#As of 1.4 can now set the age of a ruler
define_ruler = {
    adm = 2
    dip = 1
    mil = 2
    age = 28
}


#As of 1.10 can now set starting legitimacy of the ruler
define_ruler = {
    adm = 2
    dip = 1
    mil = 2
    claim = 75
}

#Can also set a regency council
define_ruler = {
    regency = yes
}

#For dynasty, can use specific name, ROOT, FROM, PREV, TAG, etc.
#When using dynasty with a republic it will determine the ruler's surname
define_ruler = {
    dynasty = "Habsburg"
    DIP = 5
    ADM = 3
    MIL = 3
}

#Example of turning a current military leader into a ruler
define_ruler = {
    name = "Andrea Doria"
    dynasty = "Doria"
    DIP = 5
    ADM = 5
    MIL = 5
    attach_leader = "Andrea Doria"
}

#Example of making a leader the regent
define_ruler = {
    name = "(Regent) János"
    dynasty = "Hunyadi"
    DIP = 5
    ADM = 4
    MIL = 5
    regency = yes
    attach_leader = "János Hunyadi"
}

#These were added in 1.18
define_ruler = {
    dynasty = original_dynasty
    max_random_adm = 5
    max_random_dip = 5
    max_random_mil = 5
}

define_ruler_to_general = {
    fire = 4
    shock = 4
    manuever = 3
    siege = 1
}

#Make a leader into the ruler
define_leader_to_ruler = {
    type =
    claim =
}
#Type- can use general and conquistador, assuming that admiral and explorer can also be used
#Can also use the properties fixed/ADM/DIP/MIL, just as in define_ruler, or name instead of type to use a specific leader

define_consort = {
    name = ""
    country_of_origin =
    dynasty = ""
    adm = 3
    dip = 3
    mil = 3
    female = yes
}
#country_of_origin can be a tag, scope, or event target.  Not including this represents domestic spouse
#Can use fixed = yes like with rulers and heirs

#############################
# Variables
#############################
change_variable = { which = _ value = 1 }
divide_variable = { which = _ value = 2 }
multiply_variable = { which = _ value = 2 }
set_variable = { which = _ value = 1 }

#Variable effects and triggers can now take a second variable as an argument, can also use OWNER
#Example from Captain Gars
set_variable = { which = var1 value = 5 } # var1 = 5
set_variable = { which = var2 which = var1 } # var2 = 5
change_variable = { which = var2 value = 2 } # var2 = 7
multiply_variable = { which = var1 which = var2 } # var1 = 35
divide_variable = { which = var1 value = 2 } # var1 = 17.5
subtract_variable = { which = var1 which = var2 }

#Variable effects and triggers can now take a scope as an argument
#Example from Captain Gars
multiply_variable = { which = var1 which = FROM }

export_to_variable = {
    which =
    value =
}

#Examples
export_to_variable = {
    which = prestigeVar
    value = prestige
}
export_to_variable = {
    which = stabilityVar
    value = stability
    who = FROM
}
export_to_variable = {
    which = productionEfficiencyVar
    value = modifier:production_efficiency
}
export_to_variable = {
    which = borderDistanceVar
    value = borderDistance
    who = FRA
}
#Allowed arguments for which

#Country
prestige
war_exhaustion
manpower
manpower_percentage
max_manpower
states_development
average_autonomy
average_home_autonomy
corruption
stability
treasury
land_forcelimit
naval_forcelimit
average_unrest
num_of_cities
army_tradition
navy_tradition
mercantilism
overextension_percentage
inflation
num_of_ports
patriarch_authority
piety
religious_unity
ADM
DIP
MIL
heir_adm
heir_dip
heir_mil
tolerance_to_this
trade_income_percentage
years_of_income
monthly_income
army_size
navy_size
adm_tech
dip_tech
mil_tech
border_distance
capital_distance
consort_adm
consort_dip
consort_mil
+ all global modifiers

#Province
province_trade_power
local_autonomy
base_manpower
base_production
base_tax
development
unrest
nationalism
tolerance_to_this
+ all local modifiers

#############################
# Province Effects (Province Scope)
#############################
##
#Changes in Province
##
add_base_manpower = 1
add_base_production = 1
add_base_tax = 1
add_building =  #Building name
add_construction_progress = -0.05 #have only seen this used for great projects
add_core_construction #Unsure of format, probably use "yes" as a right side argument
add_culture_construction = #probably uses "yes"
add_great_project = #specific name
add_local_autonomy = 10
add_scaled_local_adm_power = #Gives adm power based on base tax, so 0.5 would give 4 with 8 tax and 1.0 would give 8
add_scaled_local_dip_power = #Gives dip power based on base production, so 0.5 would give 4 with 8 production and 1.0 would give 8
add_scaled_local_mil_power = #Gives mil power based on base manpower, so 0.5 would give 4 with 8 manpower and 1.0 would give 8
cancel_construction = yes #have only seen this used for great projects
change_culture =  #Can use owner, specific culture, ROOT, FROM, PREV, or capital
change_province_name = #new name
create_advisor =  #adivisor type
remove_building = #building name
rename_capital = #new name
set_in_empire = no
set_local_autonomy = 25

add_building_construction = {
    building = name_of_building
    speed = 1 # 100% of original speed
    cost = 1 # 100% of original cost
}

add_institution_embracement = {
    which = ""
    value = 5
}

remove_loot = {
    who =
    amount = 5
}
#For the who you can use TAG, ROOT, FROM, PREV, enemy, ect.  Using enemy will divide the loot between all enemies present

##
#Colonial and Discovering
##
add_colonysize = 200
change_native_ferocity = -1
change_native_hostileness = -1
change_native_size = -3
create_colony = 200 #As of 1.4 this now takes the actual number of settlers
discover_country =  #discovers the provinces of the scope for the said country (i.e. ROOT, tag, etc)
multiply_colonysize = 0.5
undiscover_country = #undiscovers the provinces of the scope for the said country (i.e. ROOT, tag, etc)

##
#Control, Claims, and Cores
##
add_claim =
add_core =
add_permanent_claim =
add_territorial_core =
cede_province = #TAG, FROM, ROOT, THIS, PREV, previous_owner
change_controller =
change_siege = 20
remove_claim =
remove_core =
remove_territorial_core =

##
#Estates and Parliament
##
back_current_issue = #yes or no
remove_estate = #tooltip doesn't work, states that the province will get the estate
set_estate = #tooltip doesn't show correct influence gain
set_seat_in_parliament = yes #yes or no

##
#Flags, Modifiers, and Events
##
clr_province_flag =
province_event = { id = _ }
remove_province_modifier =
set_province_flag =

add_province_modifier = {
    name = "_"
    duration = 365
}

add_permanent_province_modifier = {
    name =
    duration = -1
}

##
#Military
##
kill_leader = #general, admiral, conquistador, explorer, random, or specific name.  Won't do anything if the leader isn't in the province

kill_units = {
    who =
    type =
    amount =
}
#who = enemy, TAG, ROOT, PREV, FROM, controller, or owner
#type = infantry, cavalry, artillery, ship type, or specific unit
#type is not needed, if not included will default to all

#Add Units, can use specific unit, tag, ROOT, etc.
artillery =
cavalry =
galley =
heavy_ship =
infantry =
light_ship =
transport =

#Can also add specific unit types
british_redcoat = #ROOT, THIS, PREV, FROM, etc
twodecker = #ROOT, THIS, PREV, FROM, etc

#The following allows you to build mercenary units.
mercenary_cavalry = #ROOT, PREV, FROM, TAG, THIS. Specific unit names don't work
mercenary_infantry =  #ROOT, PREV, FROM, TAG, THIS. Specific unit names don't work

#type = specific unit name, infantry, cavalry, light ships, etc.
#amount = how many, speed = how fast they will recruit (percentage of normal rate), cost = how much they will cost
add_unit_construction = {
    type =
    amount = 20
    speed = 0.25
    cost = 0
}
#Can also construct mercenary units by adding the following to add_unit_construction
mercenary = yes

#builds this percentage of forcelimit in the province scope
build_to_forcelimit = {
    infantry = 0.6
    cavalry = 0.3
    artillery = 0.1
}
#Same thing with ships
random_owned_province = {
    limit = {
        has_port = yes
    }
    build_to_forcelimit = {
        heavy_ship = 0.3
        light_ship = 0.3
        transport = 0.2
        galley = 0.2
    }
}

#Removes loot from the province
remove_loot = {
    who =
    amount = 5
}
#who = can use enemy to divide the loot up between all enemy countries present in the province (as vanilla does)
#Or you can use a specific tag to give the loot just to that tag.  I'm assuming that we can also use scopes, but this will need to be tested

##
#Religion
##
add_cardinal = yes
add_reform_center = #name of religion
change_religion =  #Can use owner, specific religion, ROOT, FROM, PREV, an event target, or heretic (which will change it to a random heretic religion)
change_to_secondary_religion = yes
remove_cardinal = yes
remove_reform_center = #Name of religion
send_missionary = yes

##
#Revolts and Rebels
##
add_nationalism = 10 # Add ten extra years of nationalism
add_unrest = 10
create_native = 1
create_pirate = 1
create_revolt = 2
#Can also use a specific rebel name = 1 (size of rebellion) to create rebels
#Example
noble_rebels = 1

#Another way to spawn rebels, this was used as a random list effect
spawn_rebels = {
    type = _
    size = 2
}

#Can name rebel leader, good for pretender rebel (not sure if can do dynasty, maybe second name is dynasty, i.e. Jane Grey)
spawn_rebels = {
    type =
    size = 1
    leader = "_"
}
unrest = 15 #adds progress to the rebel faction
win = yes #to give the rebels control of the province
#Can also set if the rebel leader is a female (place after leader = )
female = yes

#Can set rebel friend (tag, ROOT, FROM, etc)
spawn_rebels = {
    type =
    size = 2
    friend =
}

##
#Trade
##
add_trade_node_income = 10 #Inside trade node scope
change_trade_goods =  #Trade good type
recall_merchant = #ROOT, FROM, tag, PREV, etc

#Set a trade modifier, don't know what key = means (maybe localization)
#Example
add_trade_modifier = {
    who = root
    duration = 7300
    power = 1
    key = control_of_famagusta
}

remove_trade_modifier = {
    who =
    name =
}

#Inside a region scope, you can apply effects to all the provinces of that region

#############################
# Unknown
#############################
negative_power = #Not found in code

#############################
# Code that doesn't work or has been removed
#############################
#add_province_manpower = 1 #removed as of 1.12
#add_revolt_risk
#culture_group_union = #Removed as of 1.14
#remove_core_from = #never found in code, doesn't work, likely an old remnant