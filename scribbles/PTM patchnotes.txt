#PTMs
1.25
- has_province_modifier trigger now supports triggered province modifiers (takes triggered province modifier trigger key, checks both active and non-active triggered modifiers).
- Added has_active_triggered_province_modifier trigger (takes triggered province modifier trigger key).
- Make continent trigger support provinces (Instead of only continent tags and country tags)

# Triggers
- Add island provinces trigger.

# Commands
- Added "assert" console command to check trigger argument (if trigger evaluates false, test failure is invoked).


1.24
- Added Province Triggered Modifiers which are way faster than the global triggered modifiers. They also have on_activation and on_deactivation effects you can do fun stuff with.	


# Usermodding commands
1.25

- Updated "economy" console command to also display AI budget.
- Added "powerspend_count" console command.