################
# Any building #
################
ongoing_construction_prosperity_marker_onswitch_province_event_modifier = {
		trade_goods_size = 0.01 #Marks it in the trade mapmode
   		picture = "classical_architecture"
}
prosperity_marker_offswitch_province_event_modifier = {
		trade_goods_size = 0.01 #Marks it in the trade mapmode
		local_monthly_devastation = 0.1
   		picture = "classical_architecture"
}