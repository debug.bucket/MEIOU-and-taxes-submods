--How should the AI build it's army and navy?
NDefines.NAI.BIGSHIP_FRACTION = 0.1	--was 0.4 -- The proportion of big ships in an AI navy of light ships and big ships (for coastal sea countries, this fraction is mostly galleys)
NDefines.NAI.ARTILLERY_FRACTION = 0.20 --was 0.35 	-- Ratio of artillery to infantry AI will build
NDefines.NAI.FORCE_COMPOSITION_CHANGE_TECH_LEVEL = 30 -- Tech level at which AI will double its artillery fraction
NDefines.NAI.TRANSPORT_FRACTION = 0.25 --was 0.5 -- Max fraction of naval forcelimit that should be transports
NDefines.NAI.OVER_FORCELIMIT_AVOIDANCE_FACTOR = 100 --was 10		-- The higher this number is, the less willing the AI will be to exceed forcelimits

NDefines.NAI.TRADE_INTEREST_THRESHOLD = 4 --used to be 4									-- Number of merchants required to be a nation with trade interest

--AI budget systems

NDefines.NAI.INCOME_SAVINGS_FRACTION = 0.50 --used to be 0.25 -- AI will reserve this amount of their surplus for long-term savings
NDefines.NAI.COLONY_BUDGET_AMOUNT = 6.0										-- AI will reserve a maximum of this amount of monthly ducats for colonies (multiplied by number of colonists)
NDefines.NAI.DESIRED_SURPLUS = 0.25 --used to be 0.01											-- AI will aim for having at least this fraction of their income as surplus when they don't have large savings
NDefines.NAI.DESIRED_DEFICIT = 0.01											-- AI will try to spend this fraction of their money above their target for long term savings.
NDefines.NAI.EXTRA_SURPLUS_WHEN_NEEDING_BUILDINGS = 0.75	 --used to be 0.00				-- AI will aim for having at least this fraction of their income as additional surplus when they need buildings
NDefines.NAI.MAX_SAVINGS = 36	--used to be 60											-- AI will keep a maximum of this * their monthly income in long-term savings
NDefines.NAI.CORRUPTION_BUDGET_FRACTION = 0.01	--used to be 0.05							-- AI will spend a maximum of this fraction of monthly income on rooting out corruption
NDefines.NAI.ARMY_BUDGET_FRACTION = 0.6	--used to be 0.4									-- AI will spend a maximum of this fraction of monthly income on army maintenance (based off wartime costs)
NDefines.NAI.NAVY_BUDGET_FRACTION = 0.2	--used to be 0.4									-- AI will spend a maximum of this fraction of monthly income on navy maintenance (based off wartime costs)
NDefines.NAI.FORT_BUDGET_FRACTION = 0.01	--used to be 0.3									-- AI will spend a maximum of this fraction of monthly income on forts
NDefines.NAI.ADVISOR_BUDGET_FRACTION = 0.01 								-- AI will spend a maximum of this fraction of monthly income on advisor maintenance

NDefines.NAI.PEACE_TERMS_PROVINCE_BASE_MULT = 1.2 -- used to be 1.2
NDefines.NAI.PEACE_TERMS_ANNUL_TREATIES_BASE_MULT = 0.5 -- used to be 0.5
NDefines.NAI.PEACE_TERMS_GOLD_BASE_MULT = 1 -- used to be 0.1
	
NDefines.NAI.PEACE_TERMS_PROVINCE_NO_CB_MULT = 0.8								-- AI desire for a province is multiplied by this if it doesn't have a valid cb for it (only used when annexing, not applied to cores)
NDefines.NAI.PEACE_TERMS_PROVINCE_NOT_CULTURE_MULT = 0.5	--Used to be 0.8	-- AI desire for a province is multiplied by this if it is not the same culture
NDefines.NAI.PEACE_TERMS_PROVINCE_VASSAL_MULT = 0.75							-- AI desire for a province is multiplied by this if it would go to their vassal instead of themselves
NDefines.NAI.PEACE_TERMS_PROVINCE_REAL_ADJACENT_MULT = 1.2						-- AI desire for a province is increased by this multiplier for each owned adjacent province
NDefines.NAI.PEACE_TERMS_PROVINCE_NOT_ADJACENT_MULT = 0.5						-- AI desire for a province is multiplied by this if it is not adjacent at all (including vassals and other provinces being taken in peace)
NDefines.NAI.PEACE_TERMS_PROVINCE_NO_INTEREST_MULT = 0.75						-- AI desire for a province is multiplied by this if it is not on their conquest list
NDefines.NAI.PEACE_TERMS_PROVINCE_OVEREXTENSION_MIN_MULT = 0.75					-- AI desire for a province is multiplied by this if it has 99% overextension (not applied to cores)
NDefines.NAI.PEACE_TERMS_PROVINCE_OVEREXTENSION_MAX_MULT = 1.5					-- AI desire for a province is multiplied by this if it has 0% overextension (not applied to cores)
NDefines.NAI.PEACE_TERMS_TRADE_POWER_NO_TRADE_INTEREST_MULT = 0.25	--was 0.9	-- AI desire for transfering trade power is multiplied by this if they are not a merchant republic
NDefines.NAI.PEACE_TERMS_RETURN_CORES_NOT_FRIEND_MULT = 0.3						-- AI desire for returning core provinces is multiplied by this if they are not friends of the country core is being returned to
NDefines.NAI.PEACE_TERMS_ANNUL_TREATIES_NO_INTEREST_MULT = 0.05					-- AI desire for annuling a treaty is multiplied by this if they have no strategic interests in doing so
NDefines.NAI.PEACE_TERMS_PROVINCE_HRE_UNJUSTIFIED_MULT = 0.1					-- AI desire for a province is multiplied by this for HRE provinces if they are a member of the empire and don't have a CB, claim or core to it
NDefines.NAI.PEACE_TERMS_WAR_REPARATIONS_BASE_MULT = 1.0	-- was 0.4			-- AI desire for war reparations through peace
NDefines.NAI.PEACE_TERMS_WAR_REPARATIONS_MIN_INCOME_RATIO = 0.25 -- was 0.1		-- AI only wants war reparations if other country has at least this % of their income

NDefines.NAI.DIPLOMATIC_ACTION_SUBSIDIES_RELATIONS_FACTOR = 0.001					-- AI scoring for giving subsidies to a country based on opinion of the other country

NDefines.NAI.DIPLOMATIC_INTEREST_DISTANCE = 180									-- If border distance is greater than this, diplomatic AI will have less interest in the country

NDefines.NAI.DIPLOMATIC_ACTION_OFFER_CONDOTTIERI_ONLY_NEIGHBORS = 0
