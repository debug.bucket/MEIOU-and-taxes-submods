#An event must contain the following:
#
# A unique id
# A title
# A description text
# A picture
# A trigger: things that must be true for the event to fire
# A "mean time to happen", that is, how often the event should fire given that the trigger conditions are met, and modifiers thereto
	# Alternatively, it can be marked as "triggered only", meaning it is only triggered when another script specifically triggers it.
#One or more options for the player or AI to pick, with associated effects.

#Optionally you may include an effect that happens immediately upon the event firing rather than waiting for the player to choose an option (for example, to stop him from moving troops to a province before choosing the option that causes a revolt there). This may be visible to the player or hidden from him. 

# Placing all the construction mapmode province triggered modifiers

namespace = integrate_everyone_effects_PTM_placement

country_event = {
	id = integrate_everyone_effects_PTM_placement.11
	title = "integrate_everyone_effects_PTM_placement_event_11_title"
	desc = "integrate_everyone_effects_PTM_placement_event_11_desc"

	picture = STREET_SPEECH_eventPicture

	is_triggered_only = yes
	fire_only_once = yes
	hidden = yes

	immediate = {
		hidden_effect = {
			sub_sahara = {
				set_integrate_everyone_effect_triggered_province_modifiers
			}
		}
	}
	
	option = {
		name = "integrate_everyone_effects_PTM_placement_option_name" #this is the pointer to the localisation
		set_global_flag = integrate_everyoneEffectsPTMplaced_sub_sahara
	}
}