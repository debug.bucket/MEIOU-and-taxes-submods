namespace = dg_refugees

#01 - [Spanish] Pogrom
#02 - Letter of appeal
#03 - The [King]'s refusal
#04 - The [King]'s mercy
#11 - Refugee Decisions (hidden)
#12 - Wave of Refugees

country_event = { #The King's Mercy
	
	id = dg_refugees.004
	title = dg_refugees.004.t
	desc = dg_refugees.004.d
	picture = catholic_burning_church # pending a better choice of picture by David
	
	is_triggered_only = yes #By dg_refugees.002
	
	option = {
		name = dg_refugees.004.a
		dg_add_prestige = yes
		dg_add_legitimacy_small = yes
	}
}