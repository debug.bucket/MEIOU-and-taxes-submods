#####################
# Mandate of Heaven # 10s
#####################

#######################
# Religious Tolerance # 20s
#######################

namespace = dg_confucian
country_event = { #21 - Foreign Religions (Buddhists)
	
	id = dg_confucian.021
	title = dg_confucian.021.t
	desc = dg_confucian.021.d
	picture = all_pilgrims
	
	trigger = {
		OR = {
			religion = confucianism
			AND = {
				NOT = { has_dlc = "The Cossacks" }
				OR = {
					religion_group = pagan
					religion_group = european_pagan
					religion_group = new_world_pagan
				}
			}
		}
		NOT = { has_country_flag = buddhist_presence } #Removed upon new ruler
		NOT = { has_country_flag = buddhists_permitted }
		NOT = { has_country_modifier = religious_intolerance }
		OR = {
			AND = {
				check_variable = { which = buddhist_population value = 2 }
				NOT = { religion = confucianism }
			}
			AND = {
				religion = confucianism
				check_variable = { which = heretic_population value = 2 }
			}
		}
	}
	
	mean_time_to_happen = {
		
		months = 25
		
		modifier = {
			piety = 0.60
			factor = 0.7
		}
		modifier = {
			piety = 0.20
			factor = 0.7
		}
		modifier = {
			NOT = { piety = -0.20 }
			factor = 1.4
		}
		modifier = {
			NOT = { piety = -0.60 }
			factor = 1.4
		}
	}
	
	immediate = { set_country_flag = buddhist_presence }
	
	option = {
		name = dg_confucian.021.a #Permit their presence in our lands
		ai_chance = { factor = 40 }
		if = {
			limit = { has_country_flag = buddhists_banned }
			hidden_effect = { clr_country_flag = buddhists_banned }
			add_prestige = -3
		}
		hidden_effect = {
			set_country_flag = buddhists_permitted
			change_variable = { which = buddhists_ignored value = 1 }
		}
		custom_tooltip = permitted_religion_explained
		add_piety = -0.05
	}
	option = {
		name = dg_confucian.021.b #Do not acknowledge their presence
		ai_chance = {
			factor = 40
			modifier = {
				NOT = { religion = confucianism }
				factor = 0.5
			}
			modifier = {
				any_owned_province = {
					OR = {
						religion = buddhism
						religion = mahayana
						religion = vajrayana
					}
				}
				factor = 0.7
			}
			modifier = {
				num_of_owned_provinces_with = {
					OR = {
						religion = buddhism
						religion = mahayana
						religion = vajrayana
					}
					value = 3
				}
				factor = 0.7
			}
			modifier = {
				NOT = { check_variable = { which = buddhists_ignored value = 1 } }
				factor = 1.4
			}
			modifier = {
				check_variable = { which = buddhists_ignored value = 4 }
				factor = 0.5
			}
			modifier = {
				check_variable = { which = buddhists_ignored value = 6 }
				factor = 0.5
			}
		}
		if = {
			limit = { check_variable = { which = buddhists_ignored value = 8 } }
			add_prestige = -4
		}
		else_if = {
			limit = {
				check_variable = { which = buddhists_ignored value = 7 }
			}
			add_prestige = -3
		}
		else_if = {
			limit = {
				check_variable = { which = buddhists_ignored value = 6 }
			}
			add_prestige = -2
		}
		else_if = {
			limit = {
				check_variable = { which = buddhists_ignored value = 5 }
			}
			add_prestige = -1
		}
		else_if = {
			limit = {
				check_variable = { which = buddhists_ignored value = 4 }
			}
		}
		else_if = {
			limit = {
				check_variable = { which = buddhists_ignored value = 3 }
			}
			add_prestige = 0.5
		}
		else_if = {
			limit = {
				check_variable = { which = buddhists_ignored value = 2 }
			}
			add_prestige = 1
		}
		else_if = {
			limit = {
				check_variable = { which = buddhists_ignored value = 1 }
			}
			add_prestige = 1.5
		}
		else = {
			add_prestige = 2
		}
		hidden_effect = { change_variable = { which = buddhists_ignored value = 1 } }
	}
	option = {
		name = dg_confucian.021.c #Condemn their strange ways
		ai_chance = {
			factor = 20
			modifier = {
				has_country_flag = buddhists_banned
				factor = 4
			}
			modifier = {
				NOT = { religious_unity = 0.80 }
				factor = 2
			}
			modifier = {
				NOT = { religious_unity = 0.60 }
				factor = 2
			}
			modifier = {
				NOT = { religious_unity = 0.40 }
				factor = 2
			}
			modifier = {
				NOT = { religious_unity = 0.20 }
				factor = 2
			}
			modifier = {
				piety = 0.60
				factor = 1.4
			}
			modifier = {
				piety = 0.20
				factor = 1.4
			}
			modifier = {
				NOT = { piety = -0.20 }
				factor = 0.7
			}
			modifier = {
				NOT = { piety = -0.60 }
				factor = 0.7
			}
		}
		add_piety = 0.20
		if = {
			limit = { piety = 0.60 }
			add_country_modifier = { name = religious_intolerance duration = 1000 }
		}
		else_if = {
			limit = { piety = 0.20 }
			add_country_modifier = { name = religious_intolerance duration = 800 }
		}
		else_if = {
			limit = { piety = -0.20 }
			add_country_modifier = { name = religious_intolerance duration = 600 }
		}
		else_if = {
			limit = { piety = -0.60 }
			add_country_modifier = { name = religious_intolerance duration = 400 }
		}
		else = {
			add_country_modifier = { name = religious_intolerance duration = 200 }
		}
		hidden_effect = {
			clear_permitted_religion = yes
		}
	}
	option = {
		name = dg_confucian.021.e #Ban this religion from our lands
		trigger = { has_country_flag = dharmics_banned }
		ai_chance = { factor = 30 }
	}
}